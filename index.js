const express = require('express')
const open = require('open');
const fs = require('fs');
const path = require('path');
const app = express()
const port = 3000

let args = process.argv.slice(2);

let dir = args[0]

let files = [];

const getFilesRecursively = (directory) => {
  const filesInDirectory = fs.readdirSync(directory);
  for (const file of filesInDirectory) {
    const absolute = path.join(directory, file);
    if (fs.statSync(absolute).isDirectory()) {
        getFilesRecursively(absolute);
    } else {
        if (absolute.indexOf('_CT.nii.gz') != -1) {
          files.push(absolute);
        }
    }
  }
};

getFilesRecursively(dir)
fileIdx = -1

app.use('/', express.static(path.join(__dirname, './frontend/dist')))

app.get('/image/:name', (req, res) => {
  console.log(req.params.name)
  let imageFile = ''
  for (let i=0; i<files.length; i++) {
    let fnameBase = files[i]
    let fnameOverlay = files[i].replace('_CT','_thrbinCSF') 
    if (fnameBase.indexOf(req.params.name) != -1){
      imageFile = fnameBase
    } else if (fnameOverlay.indexOf(req.params.name) != -1){
      imageFile = fnameOverlay
    }
  }
  res.sendFile(imageFile)
})

app.get('/next', (req, res) => {
  fileIdx  = fileIdx+1
  let baseFile = files[fileIdx].split('/').splice(-1)
  let overlayFile = files[fileIdx].replace('_CT','_thrbinCSF').split('/').splice(-1)
  let j = {
    base: baseFile ,
    overlay: overlayFile
  }
  console.log(j)
  res.json(j)
})



app.listen(port, () => {
  console.log(`Example app listening at http://localhost:${port}`)
})

open(`http://localhost:${port}`);